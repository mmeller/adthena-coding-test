import {
  screen,
  waitFor,
  fireEvent
} from '@testing-library/react';

export async function waitForPageToBeLoaded() {
  return waitFor(() => expect(screen.queryByTestId('users')).not.toBeEmptyDOMElement());
}

export function filterByUserName(username) {
  fireEvent.change(
    screen.queryByTestId('username-filter'),
    {
      target: {
        value: username
      }
    }
  );
}

export function wait(timeInMs) {
  return new Promise((r) => setTimeout(r, timeInMs));
}
