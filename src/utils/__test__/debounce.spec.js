import { debounce } from '@utils/debounce';
import { wait } from '@test/testUtils';

describe('debounce tests', () => {
  let debounceFn;
  let value;

  const debounceTimeout = 2000;

  beforeEach(() => {
    value = 1;

    debounceFn = debounce(() => {
      value = 2;
    }, debounceTimeout);
  });

  it('should fire callback after specific time elapse', async (done) => {
    debounceFn();
    await wait(debounceTimeout);

    expect(value).toEqual(2);
    done();
  });

  it('should not fire callback before specific time elapse', async (done) => {
    debounceFn();
    await wait(debounceTimeout / 2);

    expect(value).toEqual(1);
    done();
  });
});
