import { css } from 'styled-components';

export const Circle = (radius, color) => css`
  background-color: ${color};
  width: ${radius}px;
  height: ${radius}px;
  border-radius: 50%;
`;
