import React, { useEffect, useState, useCallback } from 'react';
import styled from 'styled-components';
import { debounce } from '@utils/debounce';

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  border: 1px solid #000;
  padding: 20px;
  
  span {
    display: block;
    margin-bottom: 5px;
  }
`;

const UserInfo = styled.div`
  border-right: 1px solid #000;
  text-align: left;
  width: 260px;
`;

const Users = styled.div`
  max-height: 300px;
  overflow: scroll;
  margin-top: 15px;
`;

const useFetch = (url) => {
  const [response, setResponse] = React.useState(null);
  const [error, setError] = React.useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(url);
        const json = await res.json();
        setResponse(json);
      } catch (exception) {
        setError(exception);
      }
    };
    fetchData();
  }, [url]);

  return { response, error };
};

const UserList = () => {
  const [filter, setFilter] = useState('');
  const [value, setValue] = useState('');
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);

    return () => {
      setIsMounted(false);
    };
  }, [setIsMounted]);

  const res = useFetch(`https://jsonplaceholder.typicode.com/users${filter ? `?username=${encodeURIComponent(filter)}` : ''}`);

  const debounceFn = debounce(async (event) => {
    if (isMounted) {
      setFilter(event.target.value);
    }
  });

  const onFilterChanged = useCallback((e) => {
    setValue(e.target.value);

    debounceFn(e);
  }, [debounceFn]);

  if (!res.response) {
    return <div>Loading...</div>;
  }
  if (res.error) {
    return <div>{res.error}</div>;
  }

  const data = res.response;

  return (
    <div>
      <div>
        Filter:
        <input
          type="text"
          onChange={onFilterChanged}
          value={value}
          placeholder="Enter username"
          data-testid="username-filter"
        />
      </div>
      <Users data-testid="users">
        {data.map((user) => (
          <Row key={user.id}>
            <UserInfo>
              <span>{`Name: ${user.name}`}</span>
              <span>{`Username: ${user.username}`}</span>
            </UserInfo>
            <div>
              <div>
                <span>{user.address.street}</span>
                <span>{user.address.suite}</span>
                <span>{user.address.city}</span>
                <span>{user.address.zipcode}</span>
              </div>
              <div>
                <span>{user.email}</span>
                <span>{user.phone}</span>
              </div>
            </div>
          </Row>
        ))}
      </Users>
    </div>
  );
};

export default UserList;
