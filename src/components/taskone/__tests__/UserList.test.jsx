import React from 'react';
import {
  render,
  act
} from '@testing-library/react';
import UserList from '@components/taskone/UserList';
import {
  waitForPageToBeLoaded,
  filterByUserName,
  wait
} from '@test/testUtils';

describe('UserList tests', () => {
  let container;

  beforeEach(() => {
    container = render(<UserList />).container;
  });

  it('shows full user list after render', async (done) => {
    await waitForPageToBeLoaded();

    expect(container).toMatchSnapshot();
    done();
  });

  it('shows initial list after typing username until reaching 5 seconds delay', async (done) => {
    await waitForPageToBeLoaded();

    filterByUserName('Bret');
    await wait(1000);

    expect(container).toMatchSnapshot();
    done();
  });

  it('shows filtered list with 5 seconds delay after typing username in the filter', async (done) => {
    await waitForPageToBeLoaded();
    await act(async () => {
      filterByUserName('Bret');
      await wait(6000);
    });

    expect(container).toMatchSnapshot();
    done();
  }, 10000);
});
