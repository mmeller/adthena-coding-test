import React, { useCallback } from 'react';
import styled from 'styled-components';
import PropsType from 'prop-types';

const ButtonContainer = styled.div`
  padding: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => (props.outlined ? 'white' : 'aliceblue')};
  border: ${(props) => (props.outlined ? '1px solid blue' : '')};
  width: 150px;
  border-radius: 5px;
  color: #3333cc;

  &:hover {
    background-color: aqua;
  }
`;

const Button = (({ label, onClick, isOutlined }) => {
  const clickCallback = useCallback((event) => {
    onClick(event);
  }, [onClick]);

  return (
    <ButtonContainer data-testid="button" onClick={clickCallback} outlined={isOutlined}>
      {label}
    </ButtonContainer>
  );
});

Button.propTypes = {
  label: PropsType.string.isRequired,
  onClick: PropsType.func,
  isOutlined: PropsType.bool,
};

Button.defaultProps = {
  onClick: () => {},
  isOutlined: false
};

export default Button;
