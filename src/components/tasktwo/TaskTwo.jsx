import React, { useCallback, useState } from 'react';
import {
  Content,
  DesignList,
  Heading,
  StyledList,
  Task
} from '@components/styled-components/Task';
import Modal from '@components/tasktwo/Modal';
import LoadingSpinner from '@components/tasktwo/LoadingSpinner';
import styled from 'styled-components';
import WarningHeader from '@components/tasktwo/WarningHeader';
import ModalButtonContent from '@components/tasktwo/ModalButtonContent';

const Modals = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Section = styled.div`
  margin-bottom: 20px;
`;

const Spaced = styled.span`
  margin: 0 5px;
  font-weight: bold;
`;

const Apos = styled.span`
  margin: 0 5px;
  &:before {
    content "'";
  }

  &:after {
    content "'";
  }
`;

const List = styled.ul`
  list-style: none;
  padding: 0px 10px;
  margin-right: 10px;
  line-height: 1.1;
  height: 100px;
  overflow: scroll;
  border: 1px solid grey;
`;

const modals = [
  {
    title: 'Data loading modal: ',
    alt: 'Data loading modal',
    src: 'images/data_loading.png'
  },
  {
    title: 'Delete files modal: ',
    alt: 'Delete files modal',
    src: 'images/delete_files.png'
  },
  {
    title: 'Delete report and history modal: ',
    alt: 'Delete report and history modal',
    src: 'images/delete_report_and_history.png'
  },
];

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const currentDate = new Date();
const previousYearDate = new Date();
previousYearDate.setFullYear(currentDate.getFullYear() - 1);

const TaskTwo = () => {
  const [value, setValue] = useState('');
  const setValueCallback = useCallback((event) => {
    setValue(event.target.value);
    event.stopPropagation();
  }, [setValue]);

  const valueClickCallback = useCallback((event) => {
    event.stopPropagation();
  }, []);

  const reportModal = {
    leftButton: useCallback(() => {
      console.log(`You've clicked "Delete all" button with value ${value}`);
    }, [value]),
    rightButton: useCallback(() => {
      console.log('You\'ve clicked "Cancel" button');
    }, [])
  };

  const fileDeletionModal = {
    leftButton: useCallback(() => {
      console.log('You\'ve clicked "Yes" button');
    }, []),
    rightButton: useCallback(() => {
      console.log('You\'ve clicked "No" button');
    }, []),
  };

  return (
    <Task>
      <Heading>Task Two</Heading>
      <Modals>
        <Modal buttonLabel="Spinner">
          <LoadingSpinner />
        </Modal>
        <Modal buttonLabel="Delete file">
          <WarningHeader title="Are you sure you want to delete all your files?" />
          <ModalButtonContent
            leftButtonLabel="Yes"
            rightButtonLabel="No"
            onClickLeftButton={fileDeletionModal.leftButton}
            onClickRightButton={fileDeletionModal.rightButton}
          >
            This action cannot be undone.
          </ModalButtonContent>
        </Modal>
        <Modal buttonLabel="Delete history">
          <WarningHeader title="Are you sure you want to delete this report and its history?" />
          <ModalButtonContent
            leftButtonLabel="Delete all"
            rightButtonLabel="Cancel"
            onClickLeftButton={reportModal.leftButton}
            onClickRightButton={reportModal.rightButton}
          >
            <Section>
              If you delete the
              <Spaced>Executive metrics</Spaced>
              report, you will also delete the associated history:
            </Section>
            <List>
              {months.map((month) => <li key={month}>{`${month} ${previousYearDate.getFullYear()}`}</li>)}
            </List>
            <Section>
              Please type the word
              <Apos>Delete</Apos>
              to remove the
              <Spaced>Executive metrics</Spaced>
              report and its associated history:
            </Section>
            <input
              value={value}
              onChange={setValueCallback}
              onClick={valueClickCallback}
            />
          </ModalButtonContent>
        </Modal>
      </Modals>
      <Content>
        <h4>Complete the following task:</h4>
        <p>
          This is to assess your ability to design and build React components.
          Assume that you are part of a team starting a new project,
          a consistent design language has been set by the Design Team,
          i.e. fonts, labels, button and input styles etc. Below are
          designs for some components for you to build. With unit tests
          and without 3rd party modal packages, build the modals shown in
          these designs and have re-usability in mind as what you build
          might be re-usable in other parts of the application.
        </p>
        <p>
          {`Include unit tests. React Testing Library is already 
            configured for you in the skeleton project.`}
        </p>
        Expected:
        <StyledList type="1">
          <li>
            <strong>No </strong>
            backdrop/page overlay is required for this exercise.
          </li>
          <li>All modals should expose a mechanism to allow them to be closed.</li>
          <li>There should be a way to consume button click events on modals with buttons.</li>
          <li>Create a demo page with three buttons that trigger each of the modals to show.</li>
        </StyledList>
        <DesignList>
          {modals.map(({ title, alt, src }, index) => (
            <div key={index.toString()}>
              <label htmlFor={`img-${index}`}>
                {title}
              </label>
              <img
                alt={alt}
                src={src}
                id={`img-${index}`}
              />
            </div>
          ))}
        </DesignList>
        <hr />
        <strong>
          Feel free to use this component as a demo page for your work.
        </strong>
      </Content>
    </Task>
  );
};

export default TaskTwo;
