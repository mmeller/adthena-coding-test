import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import PropType from 'prop-types';
import Button from '@components/tasktwo/Button';

const ModalContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;
const ModalContent = styled.div`
  position: relative;
  margin: 100px auto 0;
  border: 1px solid red;
  border-radius: 10px;
  width: 75%;
  padding: 20px 0;
  background-color: white;
`;

const Modal = ({ children, buttonLabel }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const modalButtonClicked = useCallback(() => {
    setIsModalVisible(true);
  }, [setIsModalVisible]);

  const onModalClicked = useCallback(() => {
    setIsModalVisible(false);
  }, [setIsModalVisible]);

  return (
    <>
      <Button data-testid="button" label={buttonLabel} onClick={modalButtonClicked} />
      {isModalVisible && (
        <ModalContainer>
          <ModalContent data-testid="modal" onClick={onModalClicked}>
            {children}
          </ModalContent>
        </ModalContainer>
      )}
    </>
  );
};

Modal.propTypes = {
  buttonLabel: PropType.string.isRequired,
  children: PropType.any.isRequired,
};

export default Modal;
