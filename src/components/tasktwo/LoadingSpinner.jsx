import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Circle } from '@components/styled-components/Circle';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const SpinnerOutline = styled.div`
  position:relative;
  margin: 0 auto;
  ${Circle(50, 'blue')}
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
  animation: ${rotate} 2s linear infinite;
`;

const SpinnerInnerCircle = styled.div`
  ${Circle(40, 'white')}
`;
const SpinnerInnerRectangle = styled.div`
  position: absolute;
  top: 0;
  width: 10px;
  height: 10px;
  margin-top: -4px;
  background-color: white;
`;

const LoadingInfo = styled.span`
  color: blue;
`;


const LoadingSpinner = () => (
  <>
    <SpinnerOutline>
      <SpinnerInnerRectangle />
      <SpinnerInnerCircle />
    </SpinnerOutline>
    <LoadingInfo>
      Data is loading
    </LoadingInfo>
  </>
);

export default LoadingSpinner;
