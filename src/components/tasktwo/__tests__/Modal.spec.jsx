import React from 'react';
import {
  render,
  act,
  fireEvent,
  screen
} from '@testing-library/react';
import Modal from '@components/tasktwo/Modal';

it('should open modal after clicking the button', () => {
  const { container } = render(<Modal buttonLabel="button">Dummy Data</Modal>);

  act(() => {
    fireEvent.click(
      screen.queryByTestId('button')
    );
  });
  expect(container).toMatchSnapshot();
});

it('should close modal after clicking on it', () => {
  const { container } = render(<Modal buttonLabel="button">Dummy Data</Modal>);

  act(() => {
    fireEvent.click(
      screen.queryByTestId('button')
    );
  });
  act(() => {
    fireEvent.click(
      screen.queryByTestId('modal')
    );
  });

  expect(container).toMatchSnapshot();
});
