import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen
} from '@testing-library/react';
import ModalButtonContent from '@components/tasktwo/ModalButtonContent';

describe('ModalButtonContent tests', () => {
  let container;
  let value = 1;
  const leftButtonValue = 2;
  const rightButtonValue = 3;

  const clickOnButtonByIndex = async (index) => act(async () => {
    const buttons = await screen.findAllByTestId('button');
    fireEvent.click(buttons[index]);
  });

  beforeEach(() => {
    container = render(
      <ModalButtonContent
        leftButtonLabel="First"
        rightButtonLabel="Second"
        onClickLeftButton={() => {
          value = leftButtonValue;
        }}
        onClickRightButton={() => {
          value = rightButtonValue;
        }}
      >
        Label
      </ModalButtonContent>
    ).container;
  });

  it('shows content properly', () => {
    expect(container).toMatchSnapshot();
  });

  it('fires callback when clicking on left button', async (done) => {
    await clickOnButtonByIndex(0);

    expect(value).toEqual(leftButtonValue);
    done();
  });

  it('fires callback when clicking on right button', async (done) => {
    await clickOnButtonByIndex(1);

    expect(value).toEqual(rightButtonValue);
    done();
  });
});
