import React from 'react';
import { render } from '@testing-library/react';
import WarningHeader from '@components/tasktwo/WarningHeader';

it('shows content properly', () => {
  const { container } = render(<WarningHeader title="Dummy header" />);

  expect(container).toMatchSnapshot();
});
