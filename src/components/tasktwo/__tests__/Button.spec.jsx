import React from 'react';
import {
  render,
  act,
  fireEvent,
  screen
} from '@testing-library/react';
import Button from '@components/tasktwo/Button';

it('should react on click event', () => {
  let value = 1;
  render(
    <Button
      label="Sample"
      onClick={() => {
        value = 2;
      }}
    />
  );

  act(() => {
    fireEvent.click(screen.queryByTestId('button'));
  });

  expect(value).toEqual(2);
});
