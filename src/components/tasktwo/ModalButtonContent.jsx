import React from 'react';
import PropType from 'prop-types';
import styled from 'styled-components';
import { FlexRow } from '@components/styled-components/FlexRow';
import Button from '@components/tasktwo/Button';

const Buttons = styled.div`
  ${FlexRow}
  justify-content: end;
  margin-right: 5px;

  & > div {
    margin-right: 5px;
  }
`;

const Content = styled.div`
  text-align: left;
`;

const LabelContainer = styled.div`
  margin: 20px 0px 20px 5px;
`;

const ModalButtonContent = ({
  children,
  leftButtonLabel,
  rightButtonLabel,
  onClickLeftButton,
  onClickRightButton
}) => (
  <Content>
    <LabelContainer>
      {children}
    </LabelContainer>
    <Buttons>
      <Button data-testid="leftbutton" label={leftButtonLabel} onClick={onClickLeftButton} />
      <Button data-testid="rightbutton" label={rightButtonLabel} isOutlined onClick={onClickRightButton} />
    </Buttons>
  </Content>
);

ModalButtonContent.propTypes = {
  children: PropType.any.isRequired,
  leftButtonLabel: PropType.string.isRequired,
  rightButtonLabel: PropType.string.isRequired,
  onClickLeftButton: PropType.func,
  onClickRightButton: PropType.func,
};

ModalButtonContent.defaultProps = {
  onClickLeftButton: () => {},
  onClickRightButton: () => {}
};

export default ModalButtonContent;
