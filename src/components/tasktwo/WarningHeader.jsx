import React from 'react';
import styled from 'styled-components';
import { Circle } from '@components/styled-components/Circle';
import { FlexRow } from '@components/styled-components/FlexRow';
import PropType from 'prop-types';

const Row = styled.div`
  ${FlexRow}
  padding-bottom: 1px;
  margin: 10px;
  border-bottom: 1px solid grey;
`;

const Triangle = styled.div`
  width: 0px;
  height: 0px;
  border-style: solid;
  border-color: transparent transparent red;
  border-width: 0px 10px 20px 10px;
  position: relative;
  margin-right: 5px;
`;

const VerticalLine = styled.div`
position: absolute;
left: -1px;
top: 5px;
height: 4px;
border: 1px solid white;
`;

const InnerCircle = styled.div`
  position: absolute;
  top: 14px;
  left: -1px;
  ${Circle(2, 'white')};
`;

const HeaderTitle = styled.div`
  font-weight: bold;
  margin-right: 5px;
`;

const WarningHeader = ({ title }) => (
  <Row>
    <Triangle>
      <VerticalLine />
      <InnerCircle />
    </Triangle>
    <HeaderTitle>{title}</HeaderTitle>
  </Row>
);

WarningHeader.propTypes = {
  title: PropType.string.isRequired
};

export default WarningHeader;
